<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SinhVienController extends Controller
{
    public function view_all()
    {
    	return view('view_all');
    }
    public function view_one($ma_sinh_vien)
    {
    	// dd($array);
    	// print_r($array);die();

    	// return view('view_one', compact('ma_sinh_vien'));
    	return view('view_one', [
    		'ma' => $ma_sinh_vien
    	]);
    }
}
